// OmegaShooterGame. All rights reserved

using UnrealBuildTool;
using System.Collections.Generic;

public class OmegaShooterTarget : TargetRules
{
	public OmegaShooterTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "OmegaShooter" } );
	}
}

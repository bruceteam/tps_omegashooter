// OmegaShooterGame. All rights reserved

using UnrealBuildTool;
using System.Collections.Generic;

public class OmegaShooterEditorTarget : TargetRules
{
	public OmegaShooterEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "OmegaShooter" } );
	}
}

// OmegaShooterGame. All rights reserved

using UnrealBuildTool;

public class OmegaShooter : ModuleRules
{
    public OmegaShooter(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[]
        {   "Core",
            "CoreUObject",
            "Engine",
            "InputCore",
            "Niagara",
            "PhysicsCore",
            "GameplayTasks",
            "NavigationSystem"
        });

        PrivateDependencyModuleNames.AddRange(new string[] { });

        PublicIncludePaths.AddRange(new string[]
        {
            "OmegaShooter/Public/Player",
            "OmegaShooter/Public/Components",
            "OmegaShooter/Public/Dev",
            "OmegaShooter/Public/Weapon",
            "OmegaShooter/Public/UI",
            "OmegaShooter/Public/Animations",
            "OmegaShooter/Public/PickUps",
            "OmegaShooter/Public/Weapon/Components",
            "OmegaShooter/Public/AI",
            "OmegaShooter/Public/AI/Tasks",
            "OmegaShooter/Public/AI/Services",
            "OmegaShooter/Public/AI/EQS",
            "OmegaShooter/Public/AI/Decorators",
            "OmegaShooter/Public/Menu",
            "OmegaShooter/Public/Menu/UI",
        });

        // Uncomment if you are using Slate UI
        // PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });

        // Uncomment if you are using online features
        // PrivateDependencyModuleNames.Add("OnlineSubsystem");

        // To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
    }
}

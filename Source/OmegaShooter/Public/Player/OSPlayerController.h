// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "OSCoreTypes.h"
#include "OSPlayerController.generated.h"

class UOSRespawnComponent;

UCLASS()
class OMEGASHOOTER_API AOSPlayerController : public APlayerController
{
    GENERATED_BODY()

public:
    AOSPlayerController();

protected:
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Respawn")
    UOSRespawnComponent* RespawnComponent;

    virtual void BeginPlay() override;
    virtual void OnPossess(APawn* InPawn) override;
    virtual void SetupInputComponent() override;

private:
    void OnPauseGame();
    void OnMatchStateChanged(EOSMatchState State);
    void OnMuteSound();
};

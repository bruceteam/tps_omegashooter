// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "OSPlayerState.generated.h"

UCLASS()
class OMEGASHOOTER_API AOSPlayerState : public APlayerState
{
    GENERATED_BODY()

public:
    int32 GetTeamID() { return TeamID; }
    void SetTeamID(int32 ID) { TeamID = ID; }

    FLinearColor GetTeamColor() { return TeamColor; }
    void SetTeamColor(const FLinearColor& Color) { TeamColor = Color; }

    void AddKill() { ++KillsNum; }
    int32 GetKillsNum() const { return KillsNum; }
    void AddDeath() { ++DeathNum; }
    int32 GetDeathNum() const { return DeathNum; }

    void LogInfo();

private:
    int32 TeamID;
    FLinearColor TeamColor;

    int32 KillsNum = 0;
    int32 DeathNum = 0;
};

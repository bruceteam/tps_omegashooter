// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Animations/OSAnimNotify.h"
#include "OSReloadFinishedAnimNotify.generated.h"

/**
 * 
 */
UCLASS()
class OMEGASHOOTER_API UOSReloadFinishedAnimNotify : public UOSAnimNotify
{
	GENERATED_BODY()
	
};

// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Animations/OSAnimNotify.h"
#include "OSEquipFinishedAnimNotify.generated.h"


UCLASS()
class OMEGASHOOTER_API UOSEquipFinishedAnimNotify : public UOSAnimNotify
{
    GENERATED_BODY()

};

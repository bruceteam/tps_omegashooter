// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "OSCoreTypes.h"
#include "OSGameModeBase.generated.h"

class AAIController;

UCLASS()
class OMEGASHOOTER_API AOSGameModeBase : public AGameModeBase
{
    GENERATED_BODY()

public:
    AOSGameModeBase();

    FOnMatchStateChangedSignature OnMatchStateChanged;

    virtual void StartPlay() override;

    virtual UClass* GetDefaultPawnClassForController_Implementation(AController* InController) override;

    void Killed(AController* KillerController, AController* VictimController);

    int32 GetRoundTimeRemaining() { return RoundCountDown; }

    UPROPERTY(EditDefaultsOnly, Category = "Game")
    FGameData GameData;

    int32 CurrentRound = 1;

    void RespawnRequest(AController* Controller);

    virtual bool SetPause(APlayerController* PC, FCanUnpause CanUnpauseDelegate) override;
    virtual bool ClearPause() override;

protected:
    UPROPERTY(EditDefaultsOnly, Category = "Game")
    TSubclassOf<AAIController> AIControllerClass;

    UPROPERTY(EditDefaultsOnly, Category = "Game")
    TSubclassOf<APawn> AIPawnClass;

private:
    int32 RoundCountDown = 0;
    FTimerHandle GameRoundTimerHandle;
    void StartRound();
    void GameTimerUpdate();
    void SpawnBots();

    void ResetPlayers();
    void ResetOnePlayer(AController* controller);

    void CreateTeamsInfo();
    FLinearColor DetermineColorByTeamID(int32 TeamID) const;
    void SetPlayerColor(AController* controller);

    void LogPlayerInfo();

    void StartRespawn(AController* controller);
    void GameOver();

    EOSMatchState MatchState = EOSMatchState::WaitingToStart;
    void SetMatchState(EOSMatchState State);

};

// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "OSIceDamageType.generated.h"

/**
 * 
 */
UCLASS()
class OMEGASHOOTER_API UOSIceDamageType : public UDamageType
{
	GENERATED_BODY()
	
};

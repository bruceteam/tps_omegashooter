// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "OSFireDamageType.generated.h"

/**
 * 
 */
UCLASS()
class OMEGASHOOTER_API UOSFireDamageType : public UDamageType
{
	GENERATED_BODY()
	
};

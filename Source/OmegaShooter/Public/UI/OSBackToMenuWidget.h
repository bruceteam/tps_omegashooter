// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "OSBackToMenuWidget.generated.h"

class UButton;

UCLASS()
class OMEGASHOOTER_API UOSBackToMenuWidget : public UUserWidget
{
    GENERATED_BODY()

protected:
    virtual void NativeOnInitialized() override;

    UPROPERTY(meta = (BindWidget))
    UButton* BackToMenuButton;

private:
    UFUNCTION()
    void OnBackToMenu();
};

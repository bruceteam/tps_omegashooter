// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "UI/OSBaseWidget.h"
#include "OSCoreTypes.h"
#include "OSGameOverWidget.generated.h"

class UVerticalBox;
class UButton;

UCLASS()
class OMEGASHOOTER_API UOSGameOverWidget : public UOSBaseWidget
{
    GENERATED_BODY()

protected:
    UPROPERTY(meta = (BindWidget))
    UVerticalBox* PlayerStatBox;

        UPROPERTY(meta = (BindWidget))
    UButton* ResetLevelButton;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    TSubclassOf<UUserWidget> PlayerStatRowWidgetClass;

    virtual void NativeOnInitialized() override;

private:
    void OnMatchStateChanged(EOSMatchState State);
    void UpdatePlayerStat();

    UFUNCTION()
    void OnResetLevel();
};

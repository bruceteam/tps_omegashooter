// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "UI/OSBaseWidget.h"
#include "OSPauseWidget.generated.h"

class UButton;

UCLASS()
class OMEGASHOOTER_API UOSPauseWidget : public UOSBaseWidget
{
    GENERATED_BODY()
public:
    virtual void NativeOnInitialized() override;

    UPROPERTY(meta = (BindWidget))
    UButton* ClearPauseButton;

    private:
    UFUNCTION()
    void OnClearPause();
};

// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "UI/OSBaseWidget.h"
#include "OSCoreTypes.h"
#include "OSPlayerHUDwidget.generated.h"

class AOSGameModeBase;
class AOSPlayerState;
UCLASS()
class OMEGASHOOTER_API UOSPlayerHUDwidget : public UOSBaseWidget
{
    GENERATED_BODY()
public:
    UFUNCTION(BlueprintCallable, Category = "UI")
    float GetHealthPercent() const;

    UFUNCTION(BlueprintCallable, Category = "UI")
    bool GetCurrentWeaponUIData(FWeaponUIData& UIData) const;

    UFUNCTION(BlueprintCallable, Category = "UI")
    bool GetCurrentWeaponAmmoData(FAmmoData& AmmoData) const;

    UFUNCTION(BlueprintCallable, Category = "UI")
    bool IsPlayerAlive() const;

    UFUNCTION(BlueprintCallable, Category = "UI")
    bool IsPlayerSpectating() const;

    UFUNCTION(BlueprintImplementableEvent, Category = "UI")
    void OnTakeDamage();

    UFUNCTION(BlueprintCallable, Category = "UI")
    int32 GetRoundTimeRemaining() const;

    UFUNCTION(BlueprintCallable, Category = "UI")
    int32 GetRoundTimeFull() const;

    UFUNCTION(BlueprintCallable, Category = "UI")
    int32 GetCurrentRound() const;

    UFUNCTION(BlueprintCallable, Category = "UI")
    int32 GetMaxRounds() const;

    UFUNCTION(BlueprintCallable, Category = "UI")
    int32 GetKills() const;

    UFUNCTION(BlueprintCallable, Category = "UI")
    int32 GetDeaths() const;

    AOSGameModeBase* GetOSGameMode() const;
    AOSPlayerState* GetOSPlayerState() const;
    virtual void NativeOnInitialized() override;

private:
    void OnHealthChanged(float Health, float HealthDelta);
    void OnNewPawn(APawn* NewPawn);
};

// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "OSCoreTypes.h"
#include "OSHealthComponent.generated.h"

class UCameraShakeBase;
class UPhysicalMaterial;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class OMEGASHOOTER_API UOSHealthComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    UOSHealthComponent();

    float GetHealth() { return Health; }

    UFUNCTION(BlueprintCallable)
    bool IsDead() const { return FMath::IsNearlyZero(Health); }

    FOnDeathSignature OnDeath;
    FOnHealthChangedSignature OnHealthChanged;

    UFUNCTION(BlueprintCallable, Category = "Healh")
    float GetHealthPercent() const { return Health / MaxHealth; }

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HealEffect")
    bool bAutoHeal = true;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HealEffect", meta = (EditCondition = "bAutoHeal"))
    float WaitForHeal = 3.0f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HealEffect", meta = (EditCondition = "bAutoHeal"))
    float HealTick = 0.5f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HealEffect", meta = (EditCondition = "bAutoHeal"))
    float HealPerTick = 1.0f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VFX")
    TSubclassOf<UCameraShakeBase> CameraShake;

    bool TryToAddHP(float HealthAmount);
    bool isHealthFull() const;

    void ApplyDamage(float Damage, AController* InstigatedBy,AActor* DamagedActor);
protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, category = "Health")
    TMap<UPhysicalMaterial*, float> DamageModifiers;

    virtual void BeginPlay() override;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, category = "Health", meta = (ClampMin = "0.0", ClampMax = "1000.0"))
    float MaxHealth = 100.0f;

private:
    float Health = 0.0f;

    UFUNCTION()
    void OnTakeAnyDamage(
        AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

    FTimerHandle TimerForHealEffect;

    UFUNCTION(BlueprintCallable)
    void HealEffect();

    void SetHealth(float NewHealth);

    void PlayCameraShake();

    void Killed(AController* KillerController);

    UFUNCTION()
    void OnTakePointDamage(AActor* DamagedActor, float Damage, //
        class AController* InstigatedBy, FVector HitLocation,  //
        class UPrimitiveComponent* FHitComponent,              //
        FName BoneName, FVector ShotFromDirection,             //
        const class UDamageType* DamageType, AActor* DamageCauser);

    UFUNCTION()
    void OnTakeRadialDamage(AActor* DamagedActor, float Damage, //
        const class UDamageType* DamageType, FVector Origin,    //
        FHitResult HitInfo, class AController* InstigatedBy,    //
        AActor* DamageCauser);

    float GetPointDamageModifier(AActor* DamagedActor, const FName& BoneName);

    void ReportDamageEvent(float Damage, AController* InstigatedBy);
};

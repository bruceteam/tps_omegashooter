// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/OSWeaponComponent.h"
#include "OSAIWeaponComponent.generated.h"


UCLASS()
class OMEGASHOOTER_API UOSAIWeaponComponent : public UOSWeaponComponent
{
	GENERATED_BODY()
public:
      virtual void StartFire() override;
      virtual void NextWeapon() override;
};

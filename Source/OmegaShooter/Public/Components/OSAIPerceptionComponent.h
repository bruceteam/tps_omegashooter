// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Perception/AIPerceptionComponent.h"
#include "OSAIPerceptionComponent.generated.h"

UCLASS()
class OMEGASHOOTER_API UOSAIPerceptionComponent : public UAIPerceptionComponent
{
    GENERATED_BODY()

public:
    AActor* GetClosestEnemy() const;
};

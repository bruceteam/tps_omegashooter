// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "UI/OSBaseWidget.h"
#include "OSCoreTypes.h"
#include "OSMenuWidget.generated.h"

class UButton;
class UHorizontalBox;
class UOSGameInstance;
class UOSLevelItemWidget;
class USoundCue;

UCLASS()
class OMEGASHOOTER_API UOSMenuWidget : public UOSBaseWidget
{
    GENERATED_BODY()

protected:
    virtual void NativeOnInitialized() override;
    virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation) override;

    UPROPERTY(meta = (BindWidget))
    UButton* StartGameButton;

    UPROPERTY(meta = (BindWidget))
    UButton* QuitGameButton;

    UPROPERTY(meta = (BindWidget))
    UHorizontalBox* LevelItemsBox;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    TSubclassOf<UOSLevelItemWidget> LevelItemWidgetClass;

    UPROPERTY(Transient, meta = (BindWidgetAnim))
    UWidgetAnimation* HideAnimation;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
    USoundCue* StartGameSound;

private:
    UPROPERTY()
    TArray<UOSLevelItemWidget*> LevelItemWidgets;

    UFUNCTION()
    void OnSTartGame();

    UFUNCTION()
    void OnQuitGame();

    void InitLevelItems();
    void OnLevelSelected(const FLevelData& Data);
    UOSGameInstance* GetOSGameInstance() const;
};

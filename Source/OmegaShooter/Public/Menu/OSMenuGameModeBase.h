// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "OSMenuGameModeBase.generated.h"

UCLASS()
class OMEGASHOOTER_API AOSMenuGameModeBase : public AGameModeBase
{
    GENERATED_BODY()

public:
    AOSMenuGameModeBase();
};

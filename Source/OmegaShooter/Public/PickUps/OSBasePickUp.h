// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "OSBasePickUp.generated.h"

class USphereComponent;
class USoundCue;

UCLASS()
class OMEGASHOOTER_API AOSBasePickUp : public AActor
{
    GENERATED_BODY()

public:
    AOSBasePickUp();

protected:
    virtual void BeginPlay() override;

    UPROPERTY(VisibleAnyWhere, Category = "PickUp")
    USphereComponent* CollisionComponent;

    UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "PickUp")
    float RespawnTime = 5.0f;

    UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "PickUp")
    bool CouldBeTakenTest = true;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
    USoundCue* PickUpSound;

    virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

public:
    virtual void Tick(float DeltaTime) override;
    bool CouldBeTaken() const;

private:
    float RotationYaw = 0.0f;
    FTimerHandle RespawnTimerHandle;
    virtual bool GivePickUpTo(APawn* PlayerPawn);
    void PickUpWasTaken();
    void Respawn();
    void GenerateRotationYaw();
};

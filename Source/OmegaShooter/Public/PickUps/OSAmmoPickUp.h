// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "PickUps/OSBasePickUp.h"
#include "OSAmmoPickUp.generated.h"

class AOSBaseWeapon;

UCLASS()
class OMEGASHOOTER_API AOSAmmoPickUp : public AOSBasePickUp
{
    GENERATED_BODY()

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PickUp", meta = (ClampMin = "1.0", ClampMax = "10.0"))
    int32 ClipsAmount = 10;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PickUp")
    TSubclassOf<AOSBaseWeapon> WeaponType;

private:
    virtual bool GivePickUpTo(APawn* PlayerPawn) override;
};

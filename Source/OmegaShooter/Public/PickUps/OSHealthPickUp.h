// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "PickUps/OSBasePickUp.h"
#include "OSHealthPickUp.generated.h"

UCLASS()
class OMEGASHOOTER_API AOSHealthPickUp : public AOSBasePickUp
{
    GENERATED_BODY()
protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PickUp", meta = (ClampMin = "1.0", ClampMax = "100.0"))
    float HealthAmount = 50.0f;

private:
    virtual bool GivePickUpTo(APawn* PlayerPawn) override;
};

// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Player/OSBaseCharacter.h"
#include "OSAICharacter.generated.h"

class UBehaviorTree;
class UWidgetComponent;

UCLASS()
class OMEGASHOOTER_API AOSAICharacter : public AOSBaseCharacter
{
    GENERATED_BODY()
public:
    virtual void Tick(float DeltaTime) override;

    AOSAICharacter(const FObjectInitializer& ObjInit);

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AI")
    UBehaviorTree* BehaviourTreeAsset;

protected:
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
    UWidgetComponent* HealthWidgetComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AI")
    float HealthVisibilityDistance = 1000.0f;

    virtual void BeginPlay() override;
    virtual void OnHealthChanged(float Health, float HealthDelta) override;
    virtual void OnDeath() override;

    private:
    void UpdateHealthWidgetVisibility();
};

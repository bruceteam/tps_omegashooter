// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "OSAIController.generated.h"

class UOSAIPerceptionComponent;
class UOSRespawnComponent;

UCLASS()
class OMEGASHOOTER_API AOSAIController : public AAIController
{
    GENERATED_BODY()

    AOSAIController();

protected:
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "AI")
    UOSAIPerceptionComponent* OSAIPerceptionComponent;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
    FName FocusOnKeyName = "EnemyActor";

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Respawn")
    UOSRespawnComponent* RespawnComponent;

    virtual void OnPossess(APawn* InPawn) override;

    virtual void Tick(float DeltaTime) override;

private:
    AActor* GetFocusOnActor() const;
};

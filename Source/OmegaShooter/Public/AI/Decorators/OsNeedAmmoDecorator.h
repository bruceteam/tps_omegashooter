// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "OsNeedAmmoDecorator.generated.h"

class AOSBaseWeapon;

UCLASS()
class OMEGASHOOTER_API UOsNeedAmmoDecorator : public UBTDecorator
{
    GENERATED_BODY()

public:
    UOsNeedAmmoDecorator();

protected:
    virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
    TSubclassOf<AOSBaseWeapon> WeaponType;
};

// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "OSHealthPercentDecorator.generated.h"

UCLASS()
class OMEGASHOOTER_API UOSHealthPercentDecorator : public UBTDecorator
{
    GENERATED_BODY()

public:
    UOSHealthPercentDecorator();

protected:
    virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
    float HealthPercent = 0.6f;
};

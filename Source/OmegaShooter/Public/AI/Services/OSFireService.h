// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "OSFireService.generated.h"

UCLASS()
class OMEGASHOOTER_API UOSFireService : public UBTService
{
    GENERATED_BODY()

public:
    UOSFireService();

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
    FBlackboardKeySelector EnemyActorKey;

    virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};

// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "OSChangeWeaponService.generated.h"

UCLASS()
class OMEGASHOOTER_API UOSChangeWeaponService : public UBTService
{
    GENERATED_BODY()
public:
    UOSChangeWeaponService();

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI", meta = (ClampMin = "0.0", Clampmax = "1.0"))
    float Probability = 0.5f;

    virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};

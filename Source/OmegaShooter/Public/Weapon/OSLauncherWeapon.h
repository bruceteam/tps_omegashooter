// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Weapon/OSBaseWeapon.h"
#include "OSLauncherWeapon.generated.h"

class AOSProjectile;
class USoundCue;

UCLASS()
class OMEGASHOOTER_API AOSLauncherWeapon : public AOSBaseWeapon
{
    GENERATED_BODY()

public:
    virtual void StartFire() override;

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
    TSubclassOf<AOSProjectile> ProjectileClass;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
    USoundCue* NoAmmoSound;

    virtual void MakeShot() override;
};

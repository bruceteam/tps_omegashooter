// OmegaShooterGame. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "OSCoreTypes.h"
#include "OSWeaponFXComponent.generated.h"

class UNiagaraSystem;
class UPhysicalMaterial;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class OMEGASHOOTER_API UOSWeaponFXComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UOSWeaponFXComponent();

	void PlayImpactFX(const FHitResult& Hit);
protected:
	
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FX")
    FImpactData DefaultImpactData;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FX")
    TMap<UPhysicalMaterial*, FImpactData> ImpactDataMap;
};

// OmegaShooterGame. All rights reserved


#include "OSGameInstance.h"
#include "Sound/OSSoundFunctionLibrary.h"


 void UOSGameInstance::ToggleVolume() 
 {
     UOSSoundFunctionLibrary::ToggleSoundClassVolume(MasterSoundClass);
 }
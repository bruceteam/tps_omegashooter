// OmegaShooterGame. All rights reserved


#include "Components/OSCharacterMovementComponent.h"
#include "Player/OSBaseCharacter.h"

float UOSCharacterMovementComponent::GetMaxSpeed() const
{
    const float MaxSpeed = Super::GetMaxSpeed();
    const AOSBaseCharacter* Player = Cast<AOSBaseCharacter>(GetPawnOwner());
    return Player && Player->IsRunning() ? MaxSpeed * RunModifier : MaxSpeed;
}

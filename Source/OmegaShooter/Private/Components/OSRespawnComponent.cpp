// OmegaShooterGame. All rights reserved


#include "Components/OSRespawnComponent.h"
#include "OSGameModeBase.h"


UOSRespawnComponent::UOSRespawnComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UOSRespawnComponent::Respawn(int32 RespawnTime)
{
    if (!GetWorld())
        return;

    RespawnCountDown = RespawnTime;
    GetWorld()->GetTimerManager().SetTimer(RespawnTimerHandle, this, &UOSRespawnComponent::RespawnTimerUpdate, 1.0f, true);
}


void UOSRespawnComponent::RespawnTimerUpdate() 
{
    if (--RespawnCountDown ==0)
    {
        if (!GetWorld())
        return;

         GetWorld()->GetTimerManager().ClearTimer(RespawnTimerHandle);
        const auto GameMode = Cast<AOSGameModeBase>(GetWorld()->GetAuthGameMode());
         if (!GameMode)
             return;

         GameMode->RespawnRequest(Cast<AController>(GetOwner()));
    }
}

 bool UOSRespawnComponent::IsRespawnInProgress() const 
 {
     return GetWorld() && GetWorld()->GetTimerManager().IsTimerActive(RespawnTimerHandle);
 }
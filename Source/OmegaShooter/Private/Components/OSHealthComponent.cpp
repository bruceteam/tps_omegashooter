// OmegaShooterGame. All rights reserved

#include "Components/OSHealthComponent.h"
#include "GameFramework/Character.h"
#include "GameFramework/Controller.h"
#include "Player/OSBaseCharacter.h"
#include "TimerManager.h"
#include "OSGameModeBase.h"
#include "Camera/CameraShakeBase.h"
#include "Perception/AISense_Damage.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

DEFINE_LOG_CATEGORY_STATIC(LogHealthComponent, All, All);

UOSHealthComponent::UOSHealthComponent()
{
    PrimaryComponentTick.bCanEverTick = false;
}

void UOSHealthComponent::BeginPlay()
{
    Super::BeginPlay();

    check(MaxHealth > 0.0f);

    SetHealth(MaxHealth);

    AActor* ComponentOwner = GetOwner();
    if (ComponentOwner)
    {
        ComponentOwner->OnTakeAnyDamage.AddDynamic(this, &UOSHealthComponent::OnTakeAnyDamage);
        ComponentOwner->OnTakePointDamage.AddDynamic(this, &UOSHealthComponent::OnTakePointDamage);
        ComponentOwner->OnTakeRadialDamage.AddDynamic(this, &UOSHealthComponent::OnTakeRadialDamage);
    }
}

void UOSHealthComponent::OnTakeAnyDamage(
    AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
    UE_LOG(LogHealthComponent, Display, TEXT("On any Damage: %f"), Damage);
}

void UOSHealthComponent::HealEffect()
{
    SetHealth(Health + HealPerTick);

    if (isHealthFull() && GetWorld())
        GetWorld()->GetTimerManager().ClearTimer(TimerForHealEffect);
}

void UOSHealthComponent::SetHealth(float NewHealth)
{
    const auto NextHealth = FMath::Clamp(NewHealth, 0.0f, MaxHealth);
    const auto HealthDelta = NextHealth - Health;

    Health = NextHealth;
    OnHealthChanged.Broadcast(Health, HealthDelta);
}

void UOSHealthComponent::PlayCameraShake()
{
    if (IsDead())
        return;

    const auto Player = Cast<APawn>(GetOwner());
    if (!Player)
        return;

    const auto Controller = Player->GetController<APlayerController>();
    if (!Controller || !Controller->PlayerCameraManager)
        return;

    Controller->PlayerCameraManager->StartCameraShake(CameraShake);
}

bool UOSHealthComponent::TryToAddHP(float HealthAmount)
{
    if (isHealthFull())
        return false;

    SetHealth(Health + HealthAmount);
    return true;
}
bool UOSHealthComponent::isHealthFull() const
{
    return FMath::IsNearlyEqual(Health, MaxHealth);
}

void UOSHealthComponent::Killed(AController* KillerController)
{
    if (!GetWorld())
        return;

    const auto GameMode = Cast<AOSGameModeBase>(GetWorld()->GetAuthGameMode());
    if (!GameMode)
        return;

    const auto Player = Cast<APawn>(GetOwner());
    const auto VictimController = Player ? Player->GetController() : nullptr;

    GameMode->Killed(KillerController, VictimController);
}

void UOSHealthComponent::OnTakePointDamage(AActor* DamagedActor, float Damage, //
    class AController* InstigatedBy, FVector HitLocation,                      //
    class UPrimitiveComponent* FHitComponent,                                  //
    FName BoneName, FVector ShotFromDirection,                                 //
    const class UDamageType* DamageType, AActor* DamageCauser)
{
    const auto FinalDamage = Damage * GetPointDamageModifier(DamagedActor, BoneName);
    UE_LOG(LogHealthComponent, Display, TEXT("On Point Damage: %f,Final Damage: %f, bone: %s"), Damage, FinalDamage, *BoneName.ToString());
    ApplyDamage(FinalDamage, InstigatedBy, DamagedActor);
}

void UOSHealthComponent::OnTakeRadialDamage(AActor* DamagedActor, float Damage, //
    const class UDamageType* DamageType, FVector Origin,                        //
    FHitResult HitInfo, class AController* InstigatedBy,                        //
    AActor* DamageCauser)
{
    UE_LOG(LogHealthComponent, Display, TEXT("On Radial Damage: %f"), Damage);
    ApplyDamage(Damage, InstigatedBy, DamagedActor);
}

void UOSHealthComponent::ApplyDamage(float Damage, AController* InstigatedBy, AActor* DamagedActor)
{
    if (Damage <= 0.0f || IsDead() && !GetWorld())
        return;

    AOSBaseCharacter* Character = Cast<AOSBaseCharacter>(DamagedActor);
    if (Character)
    {
        if (!Character->boolCanbeDamaged)
            return;
    }

    SetHealth(Health - Damage);
    GetWorld()->GetTimerManager().ClearTimer(TimerForHealEffect);

    if (IsDead())
    {
        Killed(InstigatedBy);
        OnDeath.Broadcast();
        return;
    }
    else if (Damage > 0.0f && GetWorld() && bAutoHeal)
        GetWorld()->GetTimerManager().SetTimer(TimerForHealEffect, this, &UOSHealthComponent::HealEffect, HealTick, true, WaitForHeal);

    PlayCameraShake();
    ReportDamageEvent(Damage, InstigatedBy);
}

float UOSHealthComponent::GetPointDamageModifier(AActor* DamagedActor, const FName& BoneName)
{
    const auto Character = Cast<ACharacter>(DamagedActor);

    if (!Character ||            //
        !Character->GetMesh() || //
        !Character->GetMesh()->GetBodyInstance(BoneName))
        return 1.0f;

    const auto PhysMaterial = Character->GetMesh()->GetBodyInstance(BoneName)->GetSimplePhysicalMaterial();
    if (!PhysMaterial || !DamageModifiers.Contains(PhysMaterial))
        return 1.0f;

    return DamageModifiers[PhysMaterial];
}

void UOSHealthComponent::ReportDamageEvent(float Damage, AController* InstigatedBy)
{
    if (!InstigatedBy || !InstigatedBy->GetPawn() || !GetOwner())
        return;

    UAISense_Damage::ReportDamageEvent                   //
        (                                                //
            GetWorld(),                                  //
            GetOwner(),                                  //
            InstigatedBy->GetPawn(),                     //
            Damage,                                      //
            InstigatedBy->GetPawn()->GetActorLocation(), //
            GetOwner()->GetActorLocation()               //
        );
}
// OmegaShooterGame. All rights reserved

#include "Weapon/Components/OSWeaponFXComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"
#include "Components/DecalComponent.h"

UOSWeaponFXComponent::UOSWeaponFXComponent()
{
    PrimaryComponentTick.bCanEverTick = false;
}

void UOSWeaponFXComponent::PlayImpactFX(const FHitResult& Hit)
{
    auto ImpactData = DefaultImpactData;

    if (Hit.PhysMaterial.IsValid())
    {
        const auto PhysMat = Hit.PhysMaterial.Get();
        if (ImpactDataMap.Contains(PhysMat))
        {
            ImpactData = ImpactDataMap[PhysMat];
        }
    }

    // spawn niagara
    UNiagaraFunctionLibrary::SpawnSystemAtLocation //
        (                                          //
            GetWorld(),                            //
            ImpactData.NiagaraEffect,              //
            Hit.ImpactPoint,                       //
            Hit.ImpactNormal.Rotation()            //
        );

    // decal
    auto DecalComponent = UGameplayStatics::SpawnDecalAtLocation //
        (                                                        //
            GetWorld(),                                          //
            ImpactData.DecalData.Material,                       //
            ImpactData.DecalData.Size,                           //
            Hit.ImpactPoint,                                     //
            (Hit.ImpactNormal * -1.0f).Rotation()                //
        );

    if (DecalComponent)
    {
        DecalComponent->SetFadeOut               //
            (                                    //
                ImpactData.DecalData.LifeTime,   //
                ImpactData.DecalData.FadeOutTime //
            );
    }

    //spawnSound
    UGameplayStatics::PlaySoundAtLocation(GetWorld(), ImpactData.ImpactSound, Hit.ImpactPoint);
}
// OmegaShooterGame. All rights reserved


#include "Animations/OSAnimNotify.h"

void UOSAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) 
{
    OnNotified.Broadcast(MeshComp);
    Super::Notify(MeshComp, Animation);
}

// OmegaShooterGame. All rights reserved

#include "AI/Services/OSChangeWeaponService.h"
#include "Kismet/KismetMathLibrary.h"
#include "HeaderOSUtils.h"
#include "AIController.h"
#include "Components/OSAIWeaponComponent.h"

UOSChangeWeaponService::UOSChangeWeaponService()
{
    NodeName = "ChangeWeapon";
}

void UOSChangeWeaponService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
    float ProvabilityTick = UKismetMathLibrary::RandomFloatInRange(0.0f, 1.0f);
    if (ProvabilityTick <= Probability && Probability >0.0f)
    {
        const auto Controller = OwnerComp.GetAIOwner();
        if (Controller)
        {
            const auto WeaponComponent = OSUtils::GetOSPlayerComponent<UOSAIWeaponComponent>(Controller->GetPawn());
            if (WeaponComponent)
            {
                WeaponComponent->NextWeapon();
            }
        }
    }

    Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}
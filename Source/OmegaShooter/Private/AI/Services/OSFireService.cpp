// OmegaShooterGame. All rights reserved

#include "AI/Services/OSFireService.h"
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "HeaderOSUtils.h"
#include "Components/OSWeaponComponent.h"

UOSFireService::UOSFireService()
{
    NodeName = "Fire";
}

void UOSFireService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
    const auto Controller = OwnerComp.GetAIOwner();
    const auto Blackboard = OwnerComp.GetBlackboardComponent();

    const auto HasAim = Blackboard && Blackboard->GetValueAsObject(EnemyActorKey.SelectedKeyName);
    if (Controller)
    {
        const auto WeaponComponent = OSUtils::GetOSPlayerComponent<UOSWeaponComponent>(Controller->GetPawn());
        if (WeaponComponent)
        {
            HasAim ? WeaponComponent->StartFire() : WeaponComponent->StopFire();
        }
    }

    Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}

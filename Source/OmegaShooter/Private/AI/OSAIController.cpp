// OmegaShooterGame. All rights reserved

#include "AI/OSAIController.h"
#include "AI/OSAICharacter.h"
#include "Components/OSAIPerceptionComponent.h"
#include "Components/OSRespawnComponent.h"
#include "BehaviorTree/BlackboardComponent.h"

AOSAIController::AOSAIController() 
{
    OSAIPerceptionComponent = CreateDefaultSubobject<UOSAIPerceptionComponent>("PerceptionComponent");
    SetPerceptionComponent(*OSAIPerceptionComponent);

    bWantsPlayerState = true;

    RespawnComponent = CreateDefaultSubobject<UOSRespawnComponent>("RespawnComponent");
}

void AOSAIController::OnPossess(APawn* InPawn)
{
    Super::OnPossess(InPawn);

    const auto OSCharacter = Cast<AOSAICharacter>(InPawn);
    if (OSCharacter)
    {
        RunBehaviorTree(OSCharacter->BehaviourTreeAsset);
    }
}

void AOSAIController::Tick(float DeltaTime) 
{
    Super::Tick(DeltaTime);
    const auto AimActor = GetFocusOnActor();
    SetFocus(AimActor);
}

AActor* AOSAIController::GetFocusOnActor() const
{
    if (!GetBlackboardComponent())
        return nullptr;

    return Cast<AActor>(GetBlackboardComponent()->GetValueAsObject(FocusOnKeyName));
}

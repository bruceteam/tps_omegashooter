// OmegaShooterGame. All rights reserved

#include "AI/OSAICharacter.h"
#include "AI/OSAIController.h"
#include "Components/OSAIWeaponComponent.h"
#include "Components/OSCharacterMovementComponent.h"
#include "Components/WidgetComponent.h"
#include "UI/OSHealthBarWidget.h"
#include "Components/OSHealthComponent.h"
#include "BrainComponent.h"

AOSAICharacter::AOSAICharacter(const FObjectInitializer& ObjInit)
    : Super(ObjInit.SetDefaultSubobjectClass<UOSAIWeaponComponent>("WeaponComponent"))
{
    AutoPossessAI = EAutoPossessAI::Disabled;
    AIControllerClass = AOSAIController::StaticClass();

    bUseControllerRotationYaw = false;
    auto CharMovemet = GetCharacterMovement();
    if (GetCharacterMovement())
    {
        GetCharacterMovement()->bUseControllerDesiredRotation = true;
        GetCharacterMovement()->RotationRate = FRotator(0.0f, 200.0f, 0.0f);
    }

    HealthWidgetComponent = CreateDefaultSubobject<UWidgetComponent>("HealthWidgetComponent");
    HealthWidgetComponent->SetupAttachment(GetRootComponent());
    HealthWidgetComponent->SetWidgetSpace(EWidgetSpace::Screen);
    HealthWidgetComponent->SetDrawAtDesiredSize(true);
}

void AOSAICharacter::BeginPlay()
{
    Super::BeginPlay();

    check(HealthWidgetComponent);
}

void AOSAICharacter::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    UpdateHealthWidgetVisibility();
}

void AOSAICharacter::UpdateHealthWidgetVisibility()
{
    if (!GetWorld() ||                             //
        !GetWorld()->GetFirstPlayerController() || //
        !GetWorld()->GetFirstPlayerController()->GetPawnOrSpectator())
        return;


    const auto PlayerLocation = GetWorld()->GetFirstPlayerController()->GetPawnOrSpectator()->GetActorLocation();
    const auto Distance = FVector::Distance(PlayerLocation, GetActorLocation());
    HealthWidgetComponent->SetVisibility(Distance < HealthVisibilityDistance, true);
}

void AOSAICharacter::OnDeath()
{
    Super::OnDeath();

    const auto OSController = Cast<AOSAIController>(Controller);
    if (OSController && OSController->BrainComponent)
    {
        OSController->BrainComponent->Cleanup();
    }
}

void AOSAICharacter::OnHealthChanged(float Health, float HealthDelta)
{
    Super::OnHealthChanged(Health, HealthDelta);

    const auto HealthBarWidget = Cast<UOSHealthBarWidget>(HealthWidgetComponent->GetUserWidgetObject());
    if (!HealthBarWidget)
        return;
    HealthBarWidget->SetHealthPercent(HealthComponent->GetHealthPercent());
}
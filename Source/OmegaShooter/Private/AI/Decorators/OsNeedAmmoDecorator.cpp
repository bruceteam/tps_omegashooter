// OmegaShooterGame. All rights reserved


#include "AI/Decorators/OsNeedAmmoDecorator.h"
#include "HeaderOSUtils.h"
#include "AIController.h"
#include "Components/OSWeaponComponent.h"

UOsNeedAmmoDecorator::UOsNeedAmmoDecorator() 
{
    NodeName = "Need Ammo";
}

bool UOsNeedAmmoDecorator::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const 
{
    const auto Controller = OwnerComp.GetAIOwner();
    if (!Controller)
        return false;

    const auto WeaponComponent = OSUtils::GetOSPlayerComponent<UOSWeaponComponent>(Controller->GetPawn());
    if (!WeaponComponent)
        return false;

    return WeaponComponent->NeedAmmo(WeaponType);

}
// OmegaShooterGame. All rights reserved


#include "AI/Decorators/OSHealthPercentDecorator.h"
#include "HeaderOSUtils.h"
#include "AIController.h"
#include "Components/OSHealthComponent.h"

UOSHealthPercentDecorator::UOSHealthPercentDecorator() 
{
    NodeName = "Health Percent";
}

bool UOSHealthPercentDecorator::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const 
{
    const auto Controller = OwnerComp.GetAIOwner();
    if (!Controller)
        return false;

    const auto HealthComponent = OSUtils::GetOSPlayerComponent<UOSHealthComponent>(Controller->GetPawn());
    if (!HealthComponent || HealthComponent->IsDead())
        return false;

    return HealthComponent->GetHealthPercent() <= HealthPercent;
}
// OmegaShooterGame. All rights reserved

#include "PickUps/OSBasePickUp.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

DEFINE_LOG_CATEGORY_STATIC(LogBasePickUp, All, All);

AOSBasePickUp::AOSBasePickUp()
{
    PrimaryActorTick.bCanEverTick = true;

    CollisionComponent = CreateDefaultSubobject<USphereComponent>("CollisionComponent");
    CollisionComponent->InitSphereRadius(50.0f);
    CollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
    CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
    SetRootComponent(CollisionComponent);
}

void AOSBasePickUp::BeginPlay()
{
    Super::BeginPlay();

    check(CollisionComponent);

    GenerateRotationYaw();
}

void AOSBasePickUp::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    AddActorLocalRotation(FRotator(0.0f, RotationYaw, 0.0f));
}

bool AOSBasePickUp::CouldBeTaken() const
{
    return !GetWorldTimerManager().IsTimerActive(RespawnTimerHandle);
}

void AOSBasePickUp::NotifyActorBeginOverlap(AActor* OtherActor)
{
    Super::NotifyActorBeginOverlap(OtherActor);

    const auto Pawn = Cast<APawn>(OtherActor);
    if (Pawn)
    {
        if (GivePickUpTo(Pawn))
        {
            PickUpWasTaken();
        }
    }
}

bool AOSBasePickUp::GivePickUpTo(APawn* PlayerPawn)
{
    return false;
}

void AOSBasePickUp::PickUpWasTaken()
{
    UGameplayStatics::PlaySoundAtLocation(GetWorld(), PickUpSound, GetActorLocation());

    CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
    if (GetRootComponent())
    {
        GetRootComponent()->SetVisibility(false, true);
    }

    GetWorldTimerManager().SetTimer(RespawnTimerHandle, this, &AOSBasePickUp::Respawn, RespawnTime);
}

void AOSBasePickUp::Respawn()
{
    GenerateRotationYaw();

    if (GetRootComponent())
    {
        CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
        GetRootComponent()->SetVisibility(true, true);
    }
}

void AOSBasePickUp::GenerateRotationYaw() 
{
    const auto Direction = FMath::RandBool() ? 1.0f : -1.0f;

    RotationYaw = FMath::RandRange(1.0f, 2.0f) * Direction;
}
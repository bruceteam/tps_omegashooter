// OmegaShooterGame. All rights reserved
#include "PickUps/OSAmmoPickUp.h"
#include "Components/OSWeaponComponent.h"
#include "Components/OSHealthComponent.h"
#include "HeaderOSUtils.h"

DEFINE_LOG_CATEGORY_STATIC(LogAmmoPickUp, All, All);

bool AOSAmmoPickUp::GivePickUpTo(APawn* PlayerPawn)
{
    const auto HealthComponent = OSUtils::GetOSPlayerComponent<UOSHealthComponent>(PlayerPawn);
    if (!HealthComponent || HealthComponent->IsDead())
        return false;

    const auto WeaponComponent = OSUtils::GetOSPlayerComponent<UOSWeaponComponent>(PlayerPawn);
    if (!WeaponComponent)
        return false;

    return WeaponComponent->TryToAddAmmo(WeaponType, ClipsAmount);
}
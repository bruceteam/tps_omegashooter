// OmegaShooterGame. All rights reserved
#include "PickUps/OSHealthPickUp.h"
#include "Components/OSHealthComponent.h"
#include "HeaderOSUtils.h"

DEFINE_LOG_CATEGORY_STATIC(LogHealthPickUp, All, All);

bool AOSHealthPickUp::GivePickUpTo(APawn* PlayerPawn)
{
    const auto HealthComponent = OSUtils::GetOSPlayerComponent<UOSHealthComponent>(PlayerPawn);
    if (!HealthComponent || HealthComponent->IsDead())
        return false;


    return HealthComponent->TryToAddHP(HealthAmount);
}
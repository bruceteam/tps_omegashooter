// OmegaShooterGame. All rights reserved


#include "UI/OSBackToMenuWidget.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "OSGameInstance.h"

DEFINE_LOG_CATEGORY_STATIC(LogOSBackToMenuWidget, All, All);

void UOSBackToMenuWidget::NativeOnInitialized() 
{
    Super::NativeOnInitialized();

    if (BackToMenuButton)
    {
        BackToMenuButton->OnClicked.AddDynamic(this, &UOSBackToMenuWidget::OnBackToMenu);
    }
}

void UOSBackToMenuWidget::OnBackToMenu() 
{
    if (!GetWorld())
        return;

    const auto OSGameInstance = GetWorld()->GetGameInstance<UOSGameInstance>();
    if (!OSGameInstance)
        return;

    if (OSGameInstance->GetStartupMenuLevelName().IsNone())
    {
        UE_LOG(LogOSBackToMenuWidget, Error, TEXT("Menu Level Name is NONE"));
        return;
    }

    UGameplayStatics::OpenLevel(this, OSGameInstance->GetStartupMenuLevelName());
}

// OmegaShooterGame. All rights reserved

#include "UI/OSGameHUD.h"
#include "Engine/Canvas.h"
#include "OSGameModeBase.h"
#include "UI/OSBaseWidget.h"

DEFINE_LOG_CATEGORY_STATIC(LogHUD, All, All);

void AOSGameHUD::BeginPlay()
{
    Super::BeginPlay();

    GameWidgets.Add(EOSMatchState::InProgress, CreateWidget<UOSBaseWidget>(GetWorld(), PlayerHUDWidgetClass));
    GameWidgets.Add(EOSMatchState::Pause, CreateWidget<UOSBaseWidget>(GetWorld(), PauseWidgetClass));
    GameWidgets.Add(EOSMatchState::GameOver, CreateWidget<UOSBaseWidget>(GetWorld(), GameOverWidgetClass));

    for (auto GameWidgetPair : GameWidgets)
    {
        const auto GameWidget = GameWidgetPair.Value;
        if (!GameWidget)
            continue;
        GameWidget->AddToViewport();
        GameWidget->SetVisibility(ESlateVisibility::Hidden);
    }

    if (GetWorld())
    {
        const auto GameMode = Cast<AOSGameModeBase>(GetWorld()->GetAuthGameMode());
        if (GameMode)
        {
            GameMode->OnMatchStateChanged.AddUObject(this, &AOSGameHUD::OnMatchStateChanged);
        }
    }
}
void AOSGameHUD::DrawHUD()
{
    Super::DrawHUD();
    // DrawCrossHair();
}

void AOSGameHUD::DrawCrossHair()
{
    const TInterval<float> Center(Canvas->SizeX * 0.5f, Canvas->SizeY * 0.5f);

    const float HalfLineSize = 10.0f;
    const float LineThickness = 2.0f;
    const FLinearColor LineColor = FLinearColor::Green;

    DrawLine(Center.Min - HalfLineSize, Center.Max, Center.Min + HalfLineSize, Center.Max, LineColor, LineThickness);
    DrawLine(Center.Min, Center.Max - HalfLineSize, Center.Min, Center.Max + HalfLineSize, LineColor, LineThickness);
}

void AOSGameHUD::OnMatchStateChanged(EOSMatchState State)
{
    if (CurrentWidget)
    {
        CurrentWidget->SetVisibility(ESlateVisibility::Hidden);
    }

    if (GameWidgets.Contains(State))
    {
        CurrentWidget = GameWidgets[State];
    }

    if (CurrentWidget)
    {
        CurrentWidget->SetVisibility(ESlateVisibility::Visible);
        CurrentWidget->Show();
    }

    UE_LOG(LogHUD, Display, TEXT("State changed: %s"), *UEnum::GetValueAsString(State));
}
// OmegaShooterGame. All rights reserved


#include "UI/OSGameOverWidget.h"
#include "OSGameModeBase.h"
#include "Player/OSPlayerState.h"
#include "UI/OSPlayerStatRowWidget.h"
#include "HeaderOSUtils.h"
#include "Components/VerticalBox.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"


void UOSGameOverWidget::NativeOnInitialized() 
{
    Super::NativeOnInitialized();

    if (GetWorld())
    {
        const auto GameMode = Cast<AOSGameModeBase>(GetWorld()->GetAuthGameMode());
        if (GameMode)
        {
            GameMode->OnMatchStateChanged.AddUObject(this, &UOSGameOverWidget::OnMatchStateChanged);
        }
    }

    if (ResetLevelButton)
    {
        ResetLevelButton->OnClicked.AddDynamic(this, &UOSGameOverWidget::OnResetLevel);
    }
}

void UOSGameOverWidget::OnMatchStateChanged(EOSMatchState State) 
{
    if (State == EOSMatchState::GameOver)
    {
        UpdatePlayerStat();
    }
}

void UOSGameOverWidget::UpdatePlayerStat() 
{
    if (!GetWorld() || !PlayerStatBox)
        return;

    PlayerStatBox->ClearChildren();

    for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
    {
        const auto Controller = It->Get();
        if (!Controller)
            continue;

        const auto PlayerState = Cast<AOSPlayerState>(Controller->PlayerState);
        if (!PlayerState)
            continue;

        const auto PlayerStatRowWidget = CreateWidget<UOSPlayerStatRowWidget>(GetWorld(), PlayerStatRowWidgetClass);
        if (!PlayerStatRowWidget)
            continue;

        PlayerStatRowWidget->SetPlayerName(FText::FromString(PlayerState->GetPlayerName()));
        PlayerStatRowWidget->SeKills(OSUtils::TextFromInt(PlayerState->GetKillsNum()));
        PlayerStatRowWidget->SetDeaths(OSUtils::TextFromInt(PlayerState->GetDeathNum()));
        PlayerStatRowWidget->SetTeam(OSUtils::TextFromInt(PlayerState->GetTeamID()));
        PlayerStatRowWidget->SetPlayerIndicatorVisibility(Controller->IsPlayerController());

        PlayerStatBox->AddChild(PlayerStatRowWidget);
    }
}

void UOSGameOverWidget::OnResetLevel() 
{
    //const FName CurrentLevelName = "TestLevel";
    const FString CurrentLevelName = UGameplayStatics::GetCurrentLevelName(this);
    UGameplayStatics::OpenLevel(this, FName(CurrentLevelName));
}
// OmegaShooterGame. All rights reserved

#include "UI/OSLevelItemWidget.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "Components/Image.h"

void UOSLevelItemWidget::NativeOnInitialized() 
{
	Super::NativeOnInitialized();

	if (LevelSelectButton)
    {
          LevelSelectButton->OnClicked.AddDynamic(this, &UOSLevelItemWidget::OnLevelItemClicked);
	}
}

void UOSLevelItemWidget::SetSelected(bool IsSelected) 
{
    if (FrameImage)
    {
        FrameImage->SetVisibility(IsSelected ? ESlateVisibility::Visible : ESlateVisibility::Hidden);
    }
}

void UOSLevelItemWidget::OnLevelItemClicked() 
{
    OnLevelSelected.Broadcast(LevelData);
}

void UOSLevelItemWidget::SetLevelData(const FLevelData& Data) 
{
    LevelData = Data;

    if (LevelNameTextBlock)
    {
        LevelNameTextBlock->SetText(FText::FromName(Data.LevelDisplayName));
    }

    if (LevelImage)
    {
        LevelImage->SetBrushFromTexture(Data.LevelThumb);
    }
}
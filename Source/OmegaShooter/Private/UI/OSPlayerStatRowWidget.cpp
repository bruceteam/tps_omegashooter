// OmegaShooterGame. All rights reserved

#include "UI/OSPlayerStatRowWidget.h"
#include "Components/TextBlock.h"
#include "Components/Image.h"

void UOSPlayerStatRowWidget::SetPlayerName(const FText& Text)
{
    if (!PlayerNameTextBlock)
        return;

    PlayerNameTextBlock->SetText(Text);
}

void UOSPlayerStatRowWidget::SeKills(const FText& Text)
{
    if (!KillsTextBlock)
        return;

    KillsTextBlock->SetText(Text);
}

void UOSPlayerStatRowWidget::SetDeaths(const FText& Text)
{
    if (!DeathsTextBlock)
        return;

    DeathsTextBlock->SetText(Text);
}

void UOSPlayerStatRowWidget::SetTeam(const FText& Text)
{
    if (!TeamTextBlock)
        return;

    TeamTextBlock->SetText(Text);
}

void UOSPlayerStatRowWidget::SetPlayerIndicatorVisibility(bool Visible) 
{
     if (!PlayerIndicatorImage)
        return;

     PlayerIndicatorImage->SetVisibility(Visible ? ESlateVisibility::Visible : ESlateVisibility::Hidden);
}
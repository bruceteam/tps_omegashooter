// OmegaShooterGame. All rights reserved


#include "UI/OSPauseWidget.h"
#include "GameFramework/GameModeBase.h"
#include "Components/Button.h"


void UOSPauseWidget::NativeOnInitialized() 
{
    Super::NativeOnInitialized();

    if (ClearPauseButton)
    {
        ClearPauseButton->OnClicked.AddDynamic(this, &UOSPauseWidget::OnClearPause);
    }
}

void UOSPauseWidget::OnClearPause() 
{
    if (!GetWorld() || !GetWorld()->GetAuthGameMode())
        return;

    GetWorld()->GetAuthGameMode()->ClearPause();
}
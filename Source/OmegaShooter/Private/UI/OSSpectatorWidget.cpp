// OmegaShooterGame. All rights reserved


#include "UI/OSSpectatorWidget.h"
#include "HeaderOSUtils.h"
#include "Components/OSRespawnComponent.h"



bool UOSSpectatorWidget::GetRespawnTime(int32& CountDownTime) const 
{
    const auto RespawnComponent = OSUtils::GetOSPlayerComponent<UOSRespawnComponent>(GetOwningPlayer());
    if (!RespawnComponent || !RespawnComponent->IsRespawnInProgress())
        return false;

    CountDownTime = RespawnComponent->GetRespawnCountDown();
    return true;
}
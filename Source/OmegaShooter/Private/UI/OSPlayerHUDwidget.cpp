// OmegaShooterGame. All rights reserved

#include "UI/OSPlayerHUDwidget.h"
#include "Components/OSHealthComponent.h"
#include "Components/OSWeaponComponent.h"
#include "OSGameModeBase.h"
#include "OSPlayerState.h"
#include "HeaderOSUtils.h"

float UOSPlayerHUDwidget::GetHealthPercent() const
{
    const auto HealthComponent = OSUtils::GetOSPlayerComponent<UOSHealthComponent>(GetOwningPlayerPawn());
    if (!HealthComponent)
        return 0.0f;

    return HealthComponent->GetHealthPercent();
}

bool UOSPlayerHUDwidget::GetCurrentWeaponUIData(FWeaponUIData& UIData) const
{
    const auto WeaponComponent = OSUtils::GetOSPlayerComponent<UOSWeaponComponent>(GetOwningPlayerPawn());
    if (!WeaponComponent)
        return false;

    return WeaponComponent->GetWeaponUIData(UIData);
}

bool UOSPlayerHUDwidget::GetCurrentWeaponAmmoData(FAmmoData& AmmoData) const
{
    const auto WeaponComponent = OSUtils::GetOSPlayerComponent<UOSWeaponComponent>(GetOwningPlayerPawn());
    if (!WeaponComponent)
        return false;
    return WeaponComponent->GetWeaponAmmoData(AmmoData);
}

bool UOSPlayerHUDwidget::IsPlayerAlive() const
{
    const auto HealthComponent = OSUtils::GetOSPlayerComponent<UOSHealthComponent>(GetOwningPlayerPawn());
    return HealthComponent && !HealthComponent->IsDead();
}

bool UOSPlayerHUDwidget::IsPlayerSpectating() const
{
    const auto Controller = GetOwningPlayer();
    return Controller && Controller->GetStateName() == NAME_Spectating;
}

void UOSPlayerHUDwidget::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (GetOwningPlayer())
    {
        GetOwningPlayer()->GetOnNewPawnNotifier().AddUObject(this, &UOSPlayerHUDwidget::OnNewPawn);
        OnNewPawn(GetOwningPlayerPawn());
    }
}

void UOSPlayerHUDwidget::OnNewPawn(APawn* NewPawn) 
{
     const auto HealthComponent = OSUtils::GetOSPlayerComponent<UOSHealthComponent>(NewPawn);
    if (HealthComponent && !HealthComponent->OnHealthChanged.IsBoundToObject(this))
    {
        HealthComponent->OnHealthChanged.AddUObject(this, &UOSPlayerHUDwidget::OnHealthChanged);
    }
}

void UOSPlayerHUDwidget::OnHealthChanged(float Health, float HealthDelta)
{
    if (HealthDelta < 0.0f)
    {
        OnTakeDamage();
    }
}

int32 UOSPlayerHUDwidget::GetRoundTimeRemaining() const
{
    const auto GameMode = GetOSGameMode();

    return GameMode ? GameMode->GetRoundTimeRemaining() : 0;
}

int32 UOSPlayerHUDwidget::GetRoundTimeFull() const
{
    const auto GameMode = GetOSGameMode();

    return GameMode ? GameMode->GameData.RoundTime : 0;
}
int32 UOSPlayerHUDwidget::GetCurrentRound() const
{
    const auto GameMode = GetOSGameMode();

    return GameMode ? GameMode->CurrentRound : 0;
}

int32 UOSPlayerHUDwidget::GetMaxRounds() const
{
    const auto GameMode = GetOSGameMode();

    return GameMode ? GameMode->GameData.RoundsNum : 0;
}

int32 UOSPlayerHUDwidget::GetKills() const
{
    const auto PlayerState = GetOSPlayerState();

    return PlayerState ? PlayerState->GetKillsNum() : 0;
}

int32 UOSPlayerHUDwidget::GetDeaths() const
{
    const auto PlayerState = GetOSPlayerState();

    return PlayerState ? PlayerState->GetDeathNum() : 0;
}

AOSGameModeBase* UOSPlayerHUDwidget::GetOSGameMode() const 
{
    return GetOwningPlayer() ? Cast<AOSGameModeBase>(GetWorld()->GetAuthGameMode()) : nullptr;
}

AOSPlayerState* UOSPlayerHUDwidget::GetOSPlayerState() const 
{
    return GetOwningPlayer() ? Cast<AOSPlayerState>(GetOwningPlayer()->PlayerState) : nullptr;
}
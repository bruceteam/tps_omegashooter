// OmegaShooterGame. All rights reserved

#include "Player/OSBaseCharacter.h"
#include "Components/OSCharacterMovementComponent.h"
#include "Components/OSHealthComponent.h"
#include "Components/TextRenderComponent.h"
#include "Components/OSWeaponComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/Controller.h"

DEFINE_LOG_CATEGORY_STATIC(BaseCharacterLog, All, All);

AOSBaseCharacter::AOSBaseCharacter(const FObjectInitializer& ObjInit)
    : Super(ObjInit.SetDefaultSubobjectClass<UOSCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
    PrimaryActorTick.bCanEverTick = true;

    HealthComponent = CreateDefaultSubobject<UOSHealthComponent>("HealthComponent");
    WeaponComponent = CreateDefaultSubobject<UOSWeaponComponent>("WeaponComponent");
}

void AOSBaseCharacter::BeginPlay()
{
    Super::BeginPlay();

    boolCanbeDamaged = true;

    check(HealthComponent);
    check(GetCharacterMovement());
    check(GetMesh());

    OnHealthChanged(HealthComponent->GetHealth(), 0.0f);
    HealthComponent->OnDeath.AddUObject(this, &AOSBaseCharacter::OnDeath);
    HealthComponent->OnHealthChanged.AddUObject(this, &AOSBaseCharacter::OnHealthChanged);
    LandedDelegate.AddDynamic(this, &AOSBaseCharacter::OnGroundLanded);
}

void AOSBaseCharacter::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

bool AOSBaseCharacter::IsRunning() const
{
    return false;
}

float AOSBaseCharacter::GetMovementDirection() const
{
    if (GetVelocity().IsZero())
        return 0.0f;
    const auto VelocityNormal = GetVelocity().GetSafeNormal();
    const auto AngleBetween = FMath::Acos(FVector::DotProduct(GetActorForwardVector(), VelocityNormal));
    const auto CrossProduct = FVector::CrossProduct(GetActorForwardVector(), VelocityNormal);
    const auto Degrees = FMath::RadiansToDegrees(AngleBetween);

    return CrossProduct.IsZero() ? Degrees : Degrees * FMath::Sign(CrossProduct.Z);
}

void AOSBaseCharacter::SetplayerColor(const FLinearColor& Color) 
{
    const auto MaterialInst = GetMesh()->CreateAndSetMaterialInstanceDynamic(0);
    if (!MaterialInst)
        return;

    MaterialInst->SetVectorParameterValue(MaterialColorName, Color);
}

void AOSBaseCharacter::OnDeath()
{
    UE_LOG(BaseCharacterLog, Display, TEXT("Player %s is dead"), *GetName());

    boolCanbeDamaged = false;

    //PlayAnimMontage(DeathAnimMontage);

    GetCharacterMovement()->DisableMovement();

    SetLifeSpan(LifeSpanOnDeath);

    GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
    WeaponComponent->StopFire();
    WeaponComponent->Zoom(false);

    GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
    GetMesh()->SetSimulatePhysics(true);

    UGameplayStatics::PlaySoundAtLocation(GetWorld(), DeathSound, GetActorLocation());
}

void AOSBaseCharacter::OnHealthChanged(float Health, float HealthDelta)
{

}

void AOSBaseCharacter::OnGroundLanded(const FHitResult& Hit) 
{
    const auto FallVelocityZ = -GetVelocity().Z;
    UE_LOG(BaseCharacterLog, Display, TEXT("On landed %f"), FallVelocityZ);

    if (FallVelocityZ < LandedDamageVelocity.X)
        return;

    const auto FinalDamage = FMath::GetMappedRangeValueClamped(LandedDamageVelocity, LandedDamage, FallVelocityZ);
    UE_LOG(BaseCharacterLog, Display, TEXT("FinalDamage %f"), FinalDamage);
    //TakeDamage(FinalDamage, FDamageEvent{}, nullptr, nullptr);
    HealthComponent->ApplyDamage(FinalDamage, nullptr, nullptr);
}


// OmegaShooterGame. All rights reserved

#include "Player/OSPlayerController.h"
#include "GameFramework/GameModeBase.h"
#include "OSGameInstance.h"
#include "OSGameModeBase.h"
#include "Components/OSRespawnComponent.h"

AOSPlayerController::AOSPlayerController()
{
    RespawnComponent = CreateDefaultSubobject<UOSRespawnComponent>("RespawnComponent");
}

void AOSPlayerController::BeginPlay()
{
    Super::BeginPlay();

    if (GetWorld())
    {
        const auto GameMode = Cast<AOSGameModeBase>(GetWorld()->GetAuthGameMode());
        if (GameMode)
        {
            GameMode->OnMatchStateChanged.AddUObject(this, &AOSPlayerController::OnMatchStateChanged);
        }
    }
}

void AOSPlayerController::OnPossess(APawn* InPawn)
{
    Super::OnPossess(InPawn);

    OnNewPawn.Broadcast(InPawn);
}

void AOSPlayerController::SetupInputComponent()
{
    Super::SetupInputComponent();
    if (!InputComponent)
        return;

    InputComponent->BindAction("PauseGame", IE_Pressed, this, &AOSPlayerController::OnPauseGame);
    InputComponent->BindAction("Mute", IE_Pressed, this, &AOSPlayerController::OnMuteSound);
}

void AOSPlayerController::OnPauseGame()
{
    if (!GetWorld() && GetWorld()->GetAuthGameMode())
        return;

    GetWorld()->GetAuthGameMode()->SetPause(this);
}

void AOSPlayerController::OnMatchStateChanged(EOSMatchState State)
{
    if (State == EOSMatchState::InProgress)
    {
        SetInputMode(FInputModeGameOnly());
        bShowMouseCursor = false;
    }
    else
    {
        SetInputMode(FInputModeUIOnly());
        bShowMouseCursor = true;
    }
}
void AOSPlayerController::OnMuteSound() 
{
    if (!GetWorld())
        return;

    const auto OSGameInstance = GetWorld()->GetGameInstance<UOSGameInstance>();
    if (!OSGameInstance)
        return;

    OSGameInstance->ToggleVolume();
}
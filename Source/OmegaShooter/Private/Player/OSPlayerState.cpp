// OmegaShooterGame. All rights reserved


#include "Player/OSPlayerState.h"

DEFINE_LOG_CATEGORY_STATIC(LogOSGameState, All, All);

void AOSPlayerState::LogInfo() 
{
	UE_LOG(LogOSGameState, Display, TEXT("TeamID = %i, Kills = %i, Deaths = %i"),TeamID, KillsNum, DeathNum);
}
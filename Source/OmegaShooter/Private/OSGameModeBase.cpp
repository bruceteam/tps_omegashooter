// OmegaShooterGame. All rights reserved

#include "OSGameModeBase.h"
#include "Player/OSBaseCharacter.h"
#include "Player/OSPlayerController.h"
#include "AIController.h"
#include "Player/OSPlayerState.h"
#include "UI/OSGameHUD.h"
#include "HeaderOSUtils.h"
#include "EngineUtils.h"
#include "OSGameInstance.h"
#include "Components/OSRespawnComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogOSGameMode, All, All);

constexpr static int32 MinRoundTimeForRespawn = 10;

AOSGameModeBase::AOSGameModeBase()
{
    DefaultPawnClass = AOSBaseCharacter::StaticClass();
    PlayerControllerClass = AOSPlayerController::StaticClass();
    HUDClass = AOSGameHUD::StaticClass();
    PlayerStateClass = AOSPlayerState::StaticClass();
}

void AOSGameModeBase::StartPlay()
{
    Super::StartPlay();

    SpawnBots();
    CreateTeamsInfo();

    CurrentRound = 1;
    StartRound();

    SetMatchState(EOSMatchState::InProgress);
}

UClass* AOSGameModeBase::GetDefaultPawnClassForController_Implementation(AController* InController)
{
    if (InController && InController->IsA<AAIController>())
    {
        return AIPawnClass;
    }
    return Super::GetDefaultPawnClassForController_Implementation(InController);
}

void AOSGameModeBase::StartRound()
{
    RoundCountDown = GameData.RoundTime;
    GetWorldTimerManager().SetTimer(GameRoundTimerHandle, this, &AOSGameModeBase::GameTimerUpdate, 1.0f, true);
}

void AOSGameModeBase::GameTimerUpdate()
{
    //UE_LOG(LogOSGameMode, Display, TEXT("Time = %i, CurrentRound = %i"), RoundCountDown, CurrentRound);
    RoundCountDown--;
    if (RoundCountDown == 0)
    {
        GetWorldTimerManager().ClearTimer(GameRoundTimerHandle);

        if (CurrentRound + 1 <= GameData.RoundsNum)
        {
            CurrentRound++;
            ResetPlayers();
            StartRound();
        }
        else
        {
            GameOver();
        }
    }
}

void AOSGameModeBase::SpawnBots()
{
    if (!GetWorld())
        return;

    for (int32 i = 0; i < GameData.PlayersNum - 1; i++)
    {
        FActorSpawnParameters SpawnInfo;
        SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

        const auto OSAIController = GetWorld()->SpawnActor<AAIController>(AIControllerClass, SpawnInfo);
        RestartPlayer(OSAIController);
    }
}

void AOSGameModeBase::ResetPlayers()
{
    if (!GetWorld())
        return;

    for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
    {
        ResetOnePlayer(It->Get());
    }
}

void AOSGameModeBase::ResetOnePlayer(AController* controller)
{
    if (controller && controller->GetPawn())
    {
        controller->GetPawn()->Reset();
    }
    RestartPlayer(controller);
    SetPlayerColor(controller);
}

void AOSGameModeBase::CreateTeamsInfo()
{
    if (!GetWorld())
        return;

    int32 TeamID = 1;
    for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
    {
        const auto Controller = It->Get();
        if (!Controller)
            continue;

        const auto PlayerState = Cast<AOSPlayerState>(Controller->PlayerState);
        if (!PlayerState)
            continue;

        PlayerState->SetTeamID(TeamID);
        PlayerState->SetTeamColor(DetermineColorByTeamID(TeamID));
        PlayerState->SetPlayerName(Controller->IsPlayerController() ? "Player" : "Bot");
        SetPlayerColor(Controller);

        TeamID = TeamID == 1 ? 2 : 1;
    }
}

FLinearColor AOSGameModeBase::DetermineColorByTeamID(int32 TeamID) const
{
    if (TeamID - 1 < GameData.TeamColors.Num())
    {
        return GameData.TeamColors[TeamID - 1];
    }
    UE_LOG(LogOSGameMode, Warning, TEXT("No color for team if: %i, set to default: %s"), TeamID, *GameData.DefaultTeamColor.ToString());

    return GameData.DefaultTeamColor;
}

void AOSGameModeBase::SetPlayerColor(AController* controller)
{
    if (!controller)
        return;

    const auto Character = Cast<AOSBaseCharacter>(controller->GetPawn());
    if (!Character)
        return;

    const auto PlayerState = Cast<AOSPlayerState>(controller->PlayerState);
    if (!PlayerState)
        return;

    Character->SetplayerColor(PlayerState->GetTeamColor());
}

void AOSGameModeBase::Killed(AController* KillerController, AController* VictimController)
{
    const auto KillerPlayerState = KillerController ? Cast<AOSPlayerState>(KillerController->PlayerState) : nullptr;
    const auto VictimPlayerState = KillerController ? Cast<AOSPlayerState>(VictimController->PlayerState) : nullptr;

    if (KillerPlayerState)
    {
        KillerPlayerState->AddKill();
    }

    if (VictimPlayerState)
    {
        VictimPlayerState->AddDeath();
    }
    StartRespawn(VictimController);
}

void AOSGameModeBase::LogPlayerInfo()
{
    if (!GetWorld())
        return;
    for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
    {
        const auto Controller = It->Get();
        if (!Controller)
            continue;

        const auto PlayerState = Cast<AOSPlayerState>(Controller->PlayerState);
        if (!PlayerState)
            continue;

        PlayerState->LogInfo();
    }
}

void AOSGameModeBase::RespawnRequest(AController* Controller)
{
    ResetOnePlayer(Controller);
}

void AOSGameModeBase::StartRespawn(AController* Controller)
{
    const auto RespawnAvailable = RoundCountDown > MinRoundTimeForRespawn + GameData.RespawnTime;
    if (!RespawnAvailable)
        true;

    const auto RespawnComponent = OSUtils::GetOSPlayerComponent<UOSRespawnComponent>(Controller);
    if (!RespawnComponent)
        return;

    RespawnComponent->Respawn(GameData.RespawnTime);
}

void AOSGameModeBase::GameOver()
{
    UE_LOG(LogOSGameMode, Display, TEXT("Game Over!"));
    LogPlayerInfo();

    for (auto Pawn : TActorRange<APawn>(GetWorld()))
    {
        if (Pawn)
        {
            Pawn->TurnOff();
            Pawn->DisableInput(nullptr);
        }
    }
    SetMatchState(EOSMatchState::GameOver);
}

void AOSGameModeBase::SetMatchState(EOSMatchState State)
{
    if (MatchState == State)
        return;

    MatchState = State;
    OnMatchStateChanged.Broadcast(MatchState);
}

bool AOSGameModeBase::SetPause(APlayerController* PC, FCanUnpause CanUnpauseDelegate)
{
    const auto PauseSet = Super::SetPause(PC, CanUnpauseDelegate);

    if (PauseSet)
    {
        SetMatchState(EOSMatchState::Pause);
    }
    return PauseSet;
}

bool AOSGameModeBase::ClearPause() 
{
    const auto PauseCleared = Super::ClearPause();

    if (PauseCleared)
    {
        SetMatchState(EOSMatchState::InProgress);
    }

    return PauseCleared;
}
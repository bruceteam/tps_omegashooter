// OmegaShooterGame. All rights reserved

#include "Menu/UI/OSMenuHUD.h"
#include "UI/OSBaseWidget.h"

void AOSMenuHUD::BeginPlay()
{
    Super::BeginPlay();

    if (MenuWidgetClass)
    {
        const auto MenuWidget = CreateWidget<UOSBaseWidget>(GetWorld(), MenuWidgetClass);
        if (MenuWidget)
        {
            MenuWidget->AddToViewport();
            MenuWidget->Show();
        }
    }
}
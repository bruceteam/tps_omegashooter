// OmegaShooterGame. All rights reserved

#include "Menu/UI/OSMenuWidget.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "OSGameInstance.h"
#include "UI/OSLevelItemWidget.h"
#include "Components/HorizontalBox.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Sound/SoundCue.h"

DEFINE_LOG_CATEGORY_STATIC(LogOSMenuWidget, All, All);

void UOSMenuWidget::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (StartGameButton)
    {
        StartGameButton->OnClicked.AddDynamic(this, &UOSMenuWidget::OnSTartGame);
    }

    if (QuitGameButton)
    {
        QuitGameButton->OnClicked.AddDynamic(this, &UOSMenuWidget::OnQuitGame);
    }

    InitLevelItems();
}

void UOSMenuWidget::OnSTartGame()
{
    PlayAnimation(HideAnimation);
    UGameplayStatics::PlaySound2D(GetWorld(), StartGameSound);
}

void UOSMenuWidget::OnQuitGame()
{
    UKismetSystemLibrary::QuitGame(this, GetOwningPlayer(), EQuitPreference::Quit, true);
}

void UOSMenuWidget::InitLevelItems()
{
    const auto OSGameIntance = GetOSGameInstance();
    if (!OSGameIntance)
        return;

    checkf(OSGameIntance->GetLevelsData().Num() != 0, TEXT("Fill Levels Array in GameInstance!"));
    if (!LevelItemsBox)
        return;

    LevelItemsBox->ClearChildren();

    for (auto LevelData : OSGameIntance->GetLevelsData())
    {
        const auto LevelItemWidget = CreateWidget<UOSLevelItemWidget>(GetWorld(), LevelItemWidgetClass);
        if (!LevelItemWidget)
            continue;

        LevelItemWidget->SetLevelData(LevelData);
        LevelItemWidget->OnLevelSelected.AddUObject(this, &UOSMenuWidget::OnLevelSelected);
        LevelItemsBox->AddChild(LevelItemWidget);
        LevelItemWidgets.Add(LevelItemWidget);
    }

    if (OSGameIntance->GetStartupLevel().LevelName.IsNone())
    {
        OnLevelSelected(OSGameIntance->GetLevelsData()[0]);
    }
    else
    {
        OnLevelSelected(OSGameIntance->GetStartupLevel());
    }
}

void UOSMenuWidget::OnLevelSelected(const FLevelData& Data)
{
    const auto OSGameIntance = GetOSGameInstance();
    if (!OSGameIntance)
        return;

    OSGameIntance->SetStartupLevel(Data);
    for (auto LevelItemWidget : LevelItemWidgets)
    {
        if (LevelItemWidget)
        {
            const auto IsSelected = Data.LevelName == LevelItemWidget->GetLevelData().LevelName;
            LevelItemWidget->SetSelected(IsSelected);
        }
    }
}

UOSGameInstance* UOSMenuWidget::GetOSGameInstance() const
{
    if (!GetWorld())
        return nullptr;

    return GetWorld()->GetGameInstance<UOSGameInstance>();
}

void UOSMenuWidget::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
    if (Animation != HideAnimation)
        return;

    const auto OSGameIntance = GetOSGameInstance();
    if (!OSGameIntance)
        return;

    UGameplayStatics::OpenLevel(this, OSGameIntance->GetStartupLevel().LevelName);
}
// OmegaShooterGame. All rights reserved


#include "Menu/OSMenuPlayerController.h"
#include "OSGameInstance.h"

 void AOSMenuPlayerController::BeginPlay() 
 {
     Super::BeginPlay();

     SetInputMode(FInputModeUIOnly());
     bShowMouseCursor = true;
 }
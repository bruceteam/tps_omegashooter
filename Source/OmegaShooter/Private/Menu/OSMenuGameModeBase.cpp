// OmegaShooterGame. All rights reserved


#include "Menu/OSMenuGameModeBase.h"
#include "Menu/OSMenuPlayerController.h"
#include "Menu/UI/OSMenuHUD.h"

AOSMenuGameModeBase::AOSMenuGameModeBase() 
{
    PlayerControllerClass = AOSMenuPlayerController::StaticClass();
    HUDClass = AOSMenuHUD::StaticClass();
}
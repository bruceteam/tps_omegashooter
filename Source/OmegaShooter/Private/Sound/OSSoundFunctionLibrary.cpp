// OmegaShooterGame. All rights reserved

#include "Sound/OSSoundFunctionLibrary.h"
#include "Sound/SoundClass.h"

DEFINE_LOG_CATEGORY_STATIC(LogOSSoundFucnLib, All, All);

void UOSSoundFunctionLibrary::SetSoundClassVolume(USoundClass* SoundClass, float Volume)
{
    if (!SoundClass)
        return;

    SoundClass->Properties.Volume = FMath::Clamp(Volume, 0.0f, 1.0f);
    UE_LOG(LogOSSoundFucnLib, Display, TEXT("Volume has changed: %s -> %f"), //
        *SoundClass->GetName(), SoundClass->Properties.Volume);
}

void UOSSoundFunctionLibrary::ToggleSoundClassVolume(USoundClass* SoundClass) 
{
    if (!SoundClass)
        return;

    const auto NextVolume = SoundClass->Properties.Volume > 0.0f ? 0.0f : 1.0f;
    SetSoundClassVolume(SoundClass, NextVolume);
}